import java.util.Scanner;

public class FinalDecryptor
{

	public static final String ALPHABET = "abcdefghijklmnopqrstuvwxyz"; // alphabet array 

	public static String decrypt(String cipherText, int shiftKey) // decryption method 
	{

		cipherText = cipherText.toLowerCase(); // organizes message in lowercase 
		String plainText = ""; // calls method for plain message 
		for (int i = 0; i < cipherText.length(); i++) // uses length of message to decrypt 
		{ 
			int charPosition = ALPHABET.indexOf(cipherText.charAt(i)); 
			int keyVal = (charPosition - shiftKey) % 26; // shifts positions of letters 
			if (keyVal < 0)
			{
				keyVal = ALPHABET.length() + keyVal; // re-shifts if value is less than 0
			}
			char replaceVal = ALPHABET.charAt(keyVal); // replaces letters 
			plainText += replaceVal; 
		}
		
		return plainText; // returns de-coded message 
	}
} 
