//*************************************
// Honor Code: The work we are submitting is a result of our own thinking and efforts.
// Cassie Bachik and Ron Davis 
// CMPSC 111 Spring 2017
// Final Project 
// May 1, 2017
//
// Purpose: To program a Caesar cipher to encrypt and/or decrypt a secret message. 
//*************************************

import java.util.Date;
import java.util.Scanner;

public class Cipher
{
	//-----------------------------
	// Main method: Program execution begins here
	//----------------------------- 

	public static void main(String[] args) 
	{
	
		Scanner scan = new Scanner(System.in); // used for input 	

		// Label output with name and date: 
		System.out.println("Cassie Bachik, Ron Davis\nFinal Project\n"+ new Date()+ "\n");

		System.out.println("Welcome to the Caesar Cipher! ");  
		System.out.println("Would you like to encrypt or decrypt a secret message? "); 
		System.out.println("Type 'encrypt' or 'decrypt' to activate the cipher. ");
		System.out.println("Type 'quit' to end the program. "); 

		while(scan.hasNext())
		{
			String command = scan.nextLine(); // reads user's command
			if(command.equals("encrypt")) // encrypts the message 
			{
				Encryptor encrypt = new Encryptor(); // activates Encryptor.java
				System.out.println("What is the message you would like to encrypt? "); 
				String plainText = new String(); // reads the message 
				plainText = scan.next(); 
				System.out.println("Your encrypted message is: " + encrypt); 
			}
			else if(command.equals("decrypt")) // decrypts the message 
			{
				FinalDecryptor decrypt = new FinalDecryptor(); //  activates FinalDecryptor.java 
				System.out.println("What is the message you would like to decrypt? "); 
				String cipherText = new String(); // reads the coded message 
				cipherText = scan.next(); 
				System.out.println("Your decrypted message is: " + decrypt);
			} 
			else if(command.equals("quit")) 
			{
				break; 
			} 
		}

		System.out.println("Thank you for using the Caesar Cipher! "); 
	}
} 

