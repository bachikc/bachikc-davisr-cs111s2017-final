import java.util.Scanner; 

public class Encryptor
{

	public static final String ALPHABET = "abcdefghijklmnopqrstuvwxyz"; // alphabet array 

	public static String encrypt(String plainText, int shiftKey) // encryption method 
	{ 
		plainText = plainText.toLowerCase(); // organizes message in lowercase 
		String cipherText = ""; // calls method for cipher text 
		for (int i = 0; i < plainText.length(); i++) // uses length of message to encrypt 
		{
			int charPosition = ALPHABET.indexOf(plainText.charAt(i)); 
			int keyVal = (shiftKey + charPosition) % 26; // shifts positions of letters 
			char replaceVal = ALPHABET.charAt(keyVal); // replaces letters 
			cipherText += replaceVal; 
		}
		
		return cipherText; // returns coded message 
	}
} 
